import Vue from "vue";
import Router from "vue-router";
import MainPage from "../pages/MainPage";
import Register from "../pages/Register";
import Login from "../pages/Login";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "MainPage",
      component: MainPage
    },
    {
      path: "/register",
      name: "Register",
      component: Register
    },
    {
      path: "/login",
      name: "Login",
      component: Login
    }
  ]
});
