let express = require('express');
let mongoose = require('mongoose');
let bodyparser = require('body-parser');
let cors = require('cors');
require("dotenv").config();
let index = require("./routes/index");
const session = require("express-session");
const MongoDBSession = require("connect-mongodb-session")(session);
const mongodbURL = process.env.MONGODB_URL;
const app = express();

mongoose.connect(mongodbURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
}).catch(error => console.log(error.reason));

const Mongostore = new MongoDBSession({
    uri: mongodbURL,
    useCreateIndex: true,
    collection: "sessions",
});

app.use(session({
    secret: mongodbURL,
    resave: true,
    saveUninitialized: false,
    store: Mongostore,
    // ttl:10,
    cookie: {
        secure: false,
    }
}));
app.use(cors());
app.use(bodyparser.json());
app.use('/',index);

app.listen(1000,()=>{
    console.log('Server started at http://localhost:1000');
});
