const Movie = require("../model/movie");

exports.add_movie = (req, res) => {
    new Movie({
        title: req.body.title,
        genre: req.body.genre,
        description: req.body.description,
        movie_length: req.body.movie_length,
        year_of_production : req.body.year_of_production,
        image: req.body.image
    })
        .save()
        .then((result) => {
            res.status(200).json({
                message: "movie added successfully"
            });
        })
        .catch((error) => {
            res.status(500).json({
                message: "movie adding failed"
            });
        });
};

exports.get_movies = (req, res) => {
    Movie.find({}).then((movie)=> {
        res.status(200).json({movie})
    }).catch((error) => {
        res.status(500).json({
            message: "something went wrong while fetching movies"
        });
    })
};

exports.get_movie_by_id = (req, res) => {
    Movie.findById(req.params.id).then((movie)=>{
        res.status(200).json({movie})
    }).catch((error) => {
        res.status(500).json({
            message: "something went wrong while fetching movie by id"
        });
    })
};

exports.delete_movie = (req, res) => {
    Movie.deleteOne({ _id: req.params.id }, function(err) {
        if (err) {
            return res.status(400).json({
                    message: "could not delete movie"
                }
            );
        } else {
            res.status(200).json({
                message: "could delete movie!"
            });
        }
    });
};

exports.edit_movie = (req,res) => {
    try{
        Movie.updateOne(
            { _id: req.params.id },
            {
                $set: req.body.movie_obj,
            }
        )
            .then((result) => {
                res.status(200).json({result,
                    success:true
                });
            })
            .catch((error) => {
                res.status(500).json({
                    message: "Couldn't udpate post!",
                    success:false
                });
            });
    }
    catch (error) {
        res.status(500).json({
            message: "Error Message",
            success: false
        });
    }
}
