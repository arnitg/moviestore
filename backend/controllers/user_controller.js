const User = require("../model/User");
const bcrypt = require("bcryptjs");
const Movie = require("../model/movie");

exports.add_user = async (req, res) => {
    try{
        //Checking if the user is already in the database
        const usernameExist = await User.findOne({ username: req.body.username });
        if (usernameExist) return res.status(400).send("username already exists");

        //HASH passwords
        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(req.body.password, salt);

        //Create a new user
        const user = new User({
            username: req.body.username,
            email: req.body.email,
            password: hashedPassword,
            role: req.body.role,
        });
        try {
            await user.save();
            res.send("registered successfully");
        } catch (err) {
            res.status(400).send(err);
        }
    }
    catch(error) {
        res.status(500).json({
            message: "register error"
        });
    }
};

//LOGIN
exports.login = async (req, res) => {
    try{
        const user = await User.findOne({username: req.body.username});
        if (!user) {
            return res.status(400).send("username or password is not found");
        }
        //Password is CORRECT
        const validPass = await bcrypt.compare(req.body.password, user.password);
        if (!validPass) {
            return res.status(400).send("username or password is not found");
        }

        req.session.user_id = user._id;
        await res.status(200).json({
            auth: true,
            user_id: user._id,
        })
    }
    catch(error) {
        res.status(500).json({
            message: "login error"
        });
    }
};

exports.add_movie = (req,res) => {
    User.findById({_id: req.body._id})
        .then(user => {
            Movie.findById({_id: req.body.movie})
                .then(movie => {
                    user.movies.push(movie);
                    user.save();
                    res.status(200).json({msg: "success"})
                }).catch(movie_err => {
                res.status(500).json({
                    message: "find movie by id error"
                });
            })
    }).catch(user_err => {
        res.status(500).json({
            message: "find user by id error"
        });
    });
};

exports.log_out = (req,res) => {
    req.session.destroy((err) => {
        console.log("session destroyed")
    });
    res.send("Log Out")
};
exports.already_logged_in = (req, res) => {
    // if (!fs.existsSync("./../config.js")) {
    //     res.json({
    //         auth: false,
    //         message: "Setup",
    //         success: false
    //     })
    // }
    try {
        if (req.session.user_id) {
            res.json({
                auth: true,
                message: "you are already logged in",
            })
        } else {
            res.json({
                auth: false,
                message: "you are not logged in",
            })
        }
    }
    catch (error) {
        res.status(500).json({
            message: "error in already_logged_in api",
        });
    }
}



