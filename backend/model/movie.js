const mongoose = require("mongoose");

const movieSchema = mongoose.Schema({
    title: { type: String, required: true },
    genre: { type: String, required: true },
    description: { type: String, required: true },
    movie_length: {type: String,required: true},
    year_of_production : {type: Date, required: true},
    image: {type: String, required: false},
    create_date: {type: Date, default: new Date()}
});

module.exports = mongoose.model("Movie", movieSchema);
