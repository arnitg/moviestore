const express = require("express");
const router = express.Router();
const movie_controller = require("../controllers/movie_controller");
const user_controller = require("../controllers/user_controller");
const is_auth = require("../config/auth");

//movies
router.post('/add_movie',is_auth, movie_controller.add_movie);
router.get('/get_all_movies',is_auth,movie_controller.get_movies);
router.get('/get_movie_by_id/:id',is_auth,movie_controller.get_movie_by_id);
router.delete('/delete_movie/:id',is_auth,movie_controller.delete_movie);

//users
router.post('/add_user',is_auth,user_controller.add_user);
router.post('/login',is_auth,user_controller.login);
router.put('/add_movie_preference',is_auth,user_controller.add_movie);
router.post('/log_out',is_auth,user_controller.log_out);
router.get('/already_logged_in',is_auth,user_controller.already_logged_in);


module.exports = router;
